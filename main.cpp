#include <iostream>
#include "compito.h"
using namespace std;


int main() {
    inizializza_registro();

    char scelta = '.', presenza;
    int giorno;
    string nome, cognome;

    while (scelta != 'u') {
        cout << "\n\n-------MENU--------\n\n";
        cout << "a) registrare una presenza," << endl;
        cout << "b) mostrare le statistiche di un alunno," << endl;
        cout << "c) mostrare le statistiche dell'intero mese," << endl;
        cout << "u) esci." << endl;
        cin >> scelta;
        switch (scelta) {
            case 'a':
                cout << "Inserire lo stato dell'alunno: (P: presente, R: ritardo, U: uscita, A: assente)";
                cin >> presenza;
                cout << "inserire il giorno (da 0 a " << GIORNI_IN_UN_MESE << "): ";
                cin >> giorno;
                if(registra_presenza(richiedi(false), richiedi(true), presenza, giorno)) {
                    cout << "ok, fatto" << endl;
                } else {
                    cout << "errore, riprova." << endl;
                }
                break;
            case 'b':
                if(!mostra_presenze(richiedi(false), richiedi(true))){
                    cout << "errore, utente non trovato.";
                }
                break;
            case 'c':
                statistiche_complessive();
                break;
            case 'u':
                scelta = 'u';
                break;
            default:
                cout << "non valido" << endl;
                break;
        }
    }
    return 0;
}

