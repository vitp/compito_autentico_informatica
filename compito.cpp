#include <iostream>
#include "compito.h"

registro_t registro;

void inizializza_registro() {
    std::cout << "inserisci il nome del mese: ";
    std::cin >> registro.nome_mese;

    for(int i = 0; i<ALUNNI_IN_UNA_CLASSE; i++){
        std::cout << "Inserisci il cognome dell'alunno numero " << i+ 1 << " : ";
        std::cin >> registro.alunni[i].cognome;
        std::cout << "Inserisci il nome dell'alunno numero " << i + 1 << " : ";
        std::cin >> registro.alunni[i].nome;
    }
}

int cerca_alunno(std::string cognome, std::string nome) {
    for(int i=0; i<ALUNNI_IN_UNA_CLASSE; i++) {
        if(registro.alunni[i].nome == nome && registro.alunni[i].cognome == cognome)
            return i;
    }

    return -1;
}

bool valida_presenza(char p) {
    return p == 'P' || p == 'A' || p == 'R' || p == 'U';
}

bool registra_presenza(std::string cognome, std::string nome, char presenza, int giorno) {
    int indice_alunno = cerca_alunno(cognome, nome);

    if(indice_alunno == -1 || !valida_presenza(presenza))
        return false;

    registro.alunni[indice_alunno].presenze[giorno] = presenza;

    return true;
}

bool mostra_presenze(int i) {
    return mostra_presenze(registro.alunni[i].cognome, registro.alunni[i].nome);
}

bool mostra_presenze(std::string cognome, std::string nome) {
    int indice_alunno = cerca_alunno(cognome, nome);
    if(indice_alunno == -1)
        return false;
    int assenze = 0, ritardi = 0, uscite = 0, presenze = 0;
    for(int i = 0; i<GIORNI_IN_UN_MESE; i++) {
        switch(registro.alunni[indice_alunno].presenze[i]){
            case 'A':
                assenze++;
                break;
            case 'R':
                ritardi++;
                break;
            case 'U':
                uscite++;
                break;
            case 'P':
                presenze++;
                break;
            default:
                break;
        }
    }

    std::cout << "L'alunno " << cognome << " " << nome << " ha totalizzato " << assenze << " assenze, " \
         << ritardi << " ritardi, " << uscite << " , uscite e " << presenze << " presenze." << std::endl;

    return true;
}

void statistiche_complessive() {
    std::cout << "Statistiche registro mese di " << registro.nome_mese << "." << std::endl;
    for(int i=0; i<ALUNNI_IN_UNA_CLASSE; i++)
        mostra_presenze(i);
}

std::string richiedi(bool nome) {
    std::string temp;
    // nome ? "nome" : "cognome"   ==    "nome" se nome è true, altrimenti "cognome"
    std::cout << "Inserire il " << (nome ? "nome" : "cognome") << " dell'alunno di cui registrare la presenza: ";
    std::cin >> temp;

    return temp;
}
