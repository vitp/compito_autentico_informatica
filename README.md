### compilazione
requisiti: cmake
```
git clone https://gitlab.com/vitp/compito_autentico_informatica compito
cd compito
ccmake . & make
```
oppure:

requisiti: gcc/g++/clang
```
git clone https://gitlab.com/vitp/compito_autentico_informatica compito
cd compito
g++ -std=c++11 -o compito main.cpp compito.h compito.cpp
# o con clang:
clang++ -std=c++11 -o compito main.cpp compito.h compito.cpp
```