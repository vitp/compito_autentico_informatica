#include <iostream>

#ifndef COMPITO_H
#define COMPITO_H

const int GIORNI_IN_UN_MESE = 31;
const int ALUNNI_IN_UNA_CLASSE = 2;

struct alunno_t {
    std::string cognome;
    std::string nome;

    // vettore di caratteri che servirà a conservare la lista di presenze dell'alunno
    // P => presenza
    // A => assenza
    // R => ritardo
    // U => uscita
    char presenze[GIORNI_IN_UN_MESE];
};

struct registro_t {
    std::string nome_mese;
    alunno_t alunni[ALUNNI_IN_UNA_CLASSE];
};

void inizializza_registro();

// la funzione restituirà la posizione dell'alunno con i dati passati nel vettore del registro
// in caso l'alunno non sia stato trovato restituirà -1
int cerca_alunno(std::string cognome, std::string nome);

// la funzione restituirà true se il char passato è una presenza valida, false in caso contrario
// presenze valide:
//      P => presenza
//      A => assenza
//      R => ritardo
//      U => uscita
// NB: le presenze *devono* essere in maiuscolo, ex: 'p' non sarà una presenza valida al contrario di 'P'
bool valida_presenza(char p);


// la funzione restituirà false in caso di mancato successo, true altrimenti
// possibili errori:
//      alunno non esistente nel vettore
//      presenza non valida
bool registra_presenza(std::string cognome, std::string nome, char presenza, int giorno);

// in base ai parametri passati verrà deciso automaticamente quale usare (overload)
// la prima funzione richiamerà la seconda.
// gli errori possono essere gli stessi di [registra_presenza]
bool mostra_presenze(int i);

bool mostra_presenze(std::string cognome, std::string nome);

void statistiche_complessive();

// "utility" per richiedere il nome o il cognome dell'alunno, passare true per richiedere il nome,
// false, per richiedere il cognome
std::string richiedi(bool nome);

#endif //COMPITO_H
